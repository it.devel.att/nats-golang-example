package entities

import "time"

type MessageType string

const UserMessage MessageType = "USER_MESSAGE"
const SystemMessage MessageType = "SYSTEM_MESSAGE"

type Message struct {
	Author     string      `json:"author"`
	Text       string      `json:"text"`
	Timestamp  time.Time   `json:"timestamp"`
	Type       MessageType `json:"type"`
	InstanceID string      `json:"instance_id"`
}
