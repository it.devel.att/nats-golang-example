### NATS Experiments

For up NATS from docker-compose
```shell script
docker-compose up
```
This will start fer nats servers in cluster with IPs:
```
127.0.0.1:4222
127.0.0.1:5222
127.0.0.1:6222
```

Open few terminals and enter

```shell script
go run cmd/pubsub/main.go -nats-addr 127.0.0.1:4222,127.0.0.1:5222,127.0.0.1:6222 -channel some.example.channel -user-name "User1"
```
```shell script
go run cmd/pubsub/main.go -nats-addr 127.0.0.1:4222,127.0.0.1:5222,127.0.0.1:6222 -channel some.example.channel -user-name "User2"
```
```shell script
go run cmd/pubsub/main.go -nats-addr 127.0.0.1:4222,127.0.0.1:5222,127.0.0.1:6222 -channel some.example.channel -user-name "User3"
```
> This will run publishing messages from users

```shell script
[420a7c66-a4c5-408e-84c1-aec19dc7ef6e][2021-12-25T22:07:43+06:00][SystemMessage]: User1 connected to channel
[f774ad1e-a92d-4247-8a84-42adc0a348bb][2021-12-25T22:07:43+06:00][SystemMessage]: User2 connected to channel
[420a7c66-a4c5-408e-84c1-aec19dc7ef6e][2021-12-25T22:07:44+06:00][User1]: Some text 1
[dfbfa620-90aa-4e4d-9893-f43d85fa59ba][2021-12-25T22:07:44+06:00][SystemMessage]: User3 connected to channel
[f774ad1e-a92d-4247-8a84-42adc0a348bb][2021-12-25T22:07:44+06:00][User2]: Some text 1
[420a7c66-a4c5-408e-84c1-aec19dc7ef6e][2021-12-25T22:07:45+06:00][User1]: Some text 2
```

### TODO JetStream with NATS
