package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"github.com/google/uuid"
	"github.com/nats-io/nats.go"

	"nats-example/pkg/entities"
)

type Config struct {
	NATSAddress      string
	SubscribeChannel string
	InstanceID       string
	UserName         string
}

type App struct {
	Config       Config
	conn         *nats.Conn
	subscription *nats.Subscription

	wg     sync.WaitGroup
	ctx    context.Context
	cancel context.CancelFunc
}

func main() {
	var err error
	instanceID, err := uuid.NewRandom()
	if err != nil {
		log.Fatal(err)
	}
	natsAddr := flag.String("nats-addr", "127.0.0.1:4222", "address of nats servers")
	subscribeChannel := flag.String("channel", "random.channel", "channel to subscribe for")
	userName := flag.String("user-name", "Anonymous", "enter your name")
	flag.Parse()

	cfg := Config{
		NATSAddress:      *natsAddr,
		SubscribeChannel: *subscribeChannel,
		InstanceID:       instanceID.String(),
		UserName:         *userName,
	}
	ctx, cancel := context.WithCancel(context.Background())
	app := App{
		Config: cfg,
		ctx:    ctx,
		cancel: cancel,
	}
	if app.conn, err = nats.Connect(cfg.NATSAddress); err != nil {
		log.Fatal(err)
	}
	if err := app.Subscribe(); err != nil {
		log.Fatal(err)
	}
	app.PublishConnectMessage()
	app.StartPublishingMessages()

	app.WaitForStop()
}

func (app *App) StartPublishingMessages() {
	app.wg.Add(1)

	go func() {
		defer app.wg.Done()

		count := 0
		for {
			select {
			case <-app.ctx.Done():
				return
			default:
				count++
				time.Sleep(time.Second)
				msg := fmt.Sprintf("Some text %v", count)
				if err := app.PublishMessage(msg, entities.UserMessage); err != nil {
					log.Println(err)
				}
			}
		}
	}()
}

func (app *App) PublishMessage(text string, t entities.MessageType) error {
	msg := entities.Message{
		Author:     app.Config.UserName,
		Text:       text,
		Type:       t,
		Timestamp:  time.Now(),
		InstanceID: app.Config.InstanceID,
	}
	byteMsg, err := json.Marshal(msg)
	if err != nil {
		log.Println(err)
	}
	return app.conn.Publish(app.Config.SubscribeChannel, byteMsg)
}

func (app *App) Subscribe() error {
	s, err := app.conn.Subscribe(app.Config.SubscribeChannel, func(msg *nats.Msg) {
		message := &entities.Message{}
		err := json.Unmarshal(msg.Data, message)
		if err != nil {
			log.Println(err)
			return
		}

		switch message.Type {
		case entities.UserMessage:
			fmt.Printf("[%v][%v][%v]: %v\n", message.InstanceID, message.Timestamp.Format(time.RFC3339), message.Author, message.Text)
		case entities.SystemMessage:
			fmt.Printf("[%v][%v][SystemMessage]: %v\n", message.InstanceID, message.Timestamp.Format(time.RFC3339), message.Text)
		}
	})
	if err != nil {
		return err
	}
	app.subscription = s
	return nil
}

func (app *App) PublishConnectMessage() error {
	return app.PublishMessage(fmt.Sprintf("%v connected to channel", app.Config.UserName), entities.SystemMessage)
}

func (app *App) PublishDisconnectMessage() error {
	return app.PublishMessage(fmt.Sprintf("%v disconnected from channel", app.Config.UserName), entities.SystemMessage)
}

func (app *App) WaitForStop() {
	sysChan := make(chan os.Signal)
	signal.Notify(sysChan, syscall.SIGTERM, syscall.SIGINT)
	<-sysChan
	app.cancel()
	app.wg.Wait()
	app.PublishDisconnectMessage()
	app.conn.Flush()
	app.conn.Close()
}
